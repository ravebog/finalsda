package com.sda.finalproject.controllers;

import com.sda.finalproject.model.Rental;
import com.sda.finalproject.model.jsons.RentalJson;
import com.sda.finalproject.services.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/rentals")
public class RentalController {

    @Autowired
    RentalService rentalService;

    @GetMapping
    public List<Rental> getAllrentals(){
        return rentalService.getAllRentals();
    }

    @GetMapping("/rental/{id}")
    public Rental getRentalById(@PathVariable Integer id){
        return rentalService.getRentalById(id);
    }

//    @PostMapping(value = "/rental", consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
@PostMapping(value = "/rental")
    public RentalJson addRental(@RequestBody RentalJson rentalJson){


        return rentalService.addRental(rentalJson);

    }

    @DeleteMapping
    @RequestMapping("/rental/{id}")
    public void deleteRentalById(@PathVariable int id){
        rentalService.deleteRentalById(id);
    }

}
