package com.sda.finalproject.controllers;

import com.sda.finalproject.model.Car;
import com.sda.finalproject.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    CarService carService;

    @GetMapping
    public List<Car> getAllCars() {
        return carService.getAllCars();
    }

    @GetMapping("/car/{id}")
    public Car getCarById(@PathVariable int id){
        return carService.getCarById(id);
    }

    @PostMapping("/car")
    public Car addCar(@RequestBody Car car) {
        return carService.addCar(car);
    }

    @DeleteMapping
    @RequestMapping("/car/{id}")
    public void deleteCarById(@PathVariable int id){
        carService.deleteCarById(id);
    }


}
