package com.sda.finalproject.login.service;


import com.sda.finalproject.login.model.User;
import com.sda.finalproject.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public boolean signup(String username, String password, String email) {

        if (userRepository.findByUsernameOrEmail(username, email)) {
            return false;
        }
        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        userRepository.save(user);
        return true;
    }

    Optional<User> login(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password);
    }


}
