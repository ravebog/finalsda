package com.sda.finalproject.login.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String roleName;


}
