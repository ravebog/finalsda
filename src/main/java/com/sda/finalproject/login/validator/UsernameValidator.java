package com.sda.finalproject.login.validator;

public class UsernameValidator implements Validator {

    public String username;

    public UsernameValidator(String username){
        this.username = username;
    }

    @Override
    public boolean validate() {
        return !username.contains(" ") && username.length()>=3;
    }
}
