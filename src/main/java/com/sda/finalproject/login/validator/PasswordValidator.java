package com.sda.finalproject.login.validator;

public class PasswordValidator implements Validator{
    private String password;
    private String passwordVerify;

    public PasswordValidator(String password, String passwordVerify) {
        this.password = password;
        this.passwordVerify = passwordVerify;
    }

    @Override
    public boolean validate() {
        return password.equals(passwordVerify) && password.length() >= 3 && password.matches(".*\\d.*");
    }
}
