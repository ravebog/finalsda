package com.sda.finalproject.login.validator;

public class EmailValidator implements Validator{
    private String email;

    public EmailValidator(String email){
        this.email=email;
    }

    @Override
    public boolean validate() {
        return !email.contains(" ") && email.matches(".*@.*\\..*");
    }
}
