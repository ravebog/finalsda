package com.sda.finalproject.login.validator;

public interface Validator {
    boolean validate();
}
