package com.sda.finalproject.login.repository;

import com.sda.finalproject.login.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Integer> {
}
