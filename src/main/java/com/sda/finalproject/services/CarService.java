package com.sda.finalproject.services;

import com.sda.finalproject.model.Car;
import com.sda.finalproject.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;


    public List<Car> getAllCars(){
        return carRepository.findAll();
    }

    public Car getCarById(Integer id){
        return carRepository.findById(id).get();
    }

    public Car addCar(Car newCar){
        return carRepository.saveAndFlush(newCar);
    }

    public void deleteCarById(Integer id){
        carRepository.deleteById(id);

    }

}
