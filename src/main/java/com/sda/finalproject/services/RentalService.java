package com.sda.finalproject.services;

import com.sda.finalproject.model.Rental;
import com.sda.finalproject.model.jsons.RentalJson;
import com.sda.finalproject.repositories.RentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RentalService {

    @Autowired
    RentalRepository rentalRepository;

    @Autowired
    ClientService clientService;

    @Autowired
    CarService carService;

    public List<Rental> getAllRentals(){
        return rentalRepository.findAll();
    }

    public Rental getRentalById(Integer id){
        return rentalRepository.findById(id).get();
    }

    public RentalJson addRental(RentalJson rentalJson){
        Rental rental = new Rental();
        rental.setPickUpDate(rentalJson.getPickUpDate());
        rental.setDropOffDate(rentalJson.getDropOffDate());

        rental.setClient(clientService.getClientById(rentalJson.getClientId()));
        rental.setCar(carService.getCarById(rentalJson.getCarId()));
        rentalRepository.saveAndFlush(rental);
        return rentalJson;

    }


    public void deleteRentalById(Integer id){
        rentalRepository.deleteById(id);
    }

}
