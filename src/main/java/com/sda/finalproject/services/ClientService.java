package com.sda.finalproject.services;

import com.sda.finalproject.model.Car;
import com.sda.finalproject.model.Client;
import com.sda.finalproject.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public List<Client> getAllClients(){
        return clientRepository.findAll();
    }

    public Client getClientById(Integer id){
        return clientRepository.findById(id).get();
    }

    public Client addClient(Client newClient){
        return clientRepository.saveAndFlush(newClient);
    }

    public void deleteClientById(Integer id){
        clientRepository.deleteById(id);

    }

}
