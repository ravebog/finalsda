package com.sda.finalproject.model;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;


public enum Status {

    AVAILABLE,
    UNAVAILABLE
}
