package com.sda.finalproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "cars")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String brand;
    private String model;
    @Column(name = "year")
    private int yearOfProduction;
    private String color;
    private long mileage;

    @Enumerated(EnumType.STRING)
    private Status status;
    private int amount;

//    @JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
    @JsonIgnore
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Rental> carRentalList=new ArrayList<>();

}
