package com.sda.finalproject.model.jsons;

import com.sda.finalproject.model.Car;
import com.sda.finalproject.model.Client;
import com.sda.finalproject.model.Status;
import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class RentalJson {

    private Integer clientId;
    private Integer carId;

    private String pickUpDate;
    private String dropOffDate;


}
